import MenuComponent from './components/MenuComponent.js'
import ModalComponent from './components/ModalComponent.js'
import DocumentStorage from './js/DocumentStorage.js'

window.app = new Vue({
    components:{
        'menu-component':MenuComponent,
        'modal-component':ModalComponent
    },
    el:'#app',
    template:`
    <div style="display:flex; flex-direction:column; height:100vh">
        <menu-component ref="menu" :items="menuEntries" :title="title" @click="onMenuClick"></menu-component>
        <div class="search-box" :style="{display:searchShow?'flex':'none'}">
            <input type="text" v-model="pattern" id="pattern">
            <button @click="search">go</button>
            <div class="btn-close" @click="closeSearch">x</div>
        </div>
        <textarea></textarea>
        <modal-component title="Open" :shown="openShown" @close="openShown=false">
            <table>
                <tr><th>name</th><th>modified</th><th>size</th></tr>
                <tr v-for="document in documents" @click="open(document.name)">
                    <td>{{document.name}}</td><td>{{document.modified}}</td><td>{{document.size}}</td>
                </tr>
            </table>
        </modal-component>
        <modal-component title="Save As" :shown="saveShown" @close="saveShown=false">
            <input type="text" placeholder="name" id="saveName" v-model="title">
            <button @click="save">OK</button>
        </modal-component>
        <input id='uploader' type='file' hidden/>
    </div>
    `,
    data(){
        return {
            menuEntries: {
                file: {
                    'open': {},
                    'save': {},
                    'save as': {},
                    'import': {},
                    'export': {},
                    'reset': {},
                    'delete': {}
                },
                edit: {
                    'syntax': {
                        'Plain': {},
                        'C-like': {},
                        'CSS': {},
                        'HTML': {},
                        'JavaScript': {},
                        'Markdown': {},
                        'PHP': {},
                        'Python': {},
                        'Shell': {},
                        'SQL': {}
                    },
                    'find': {}
                },
                view: {
                    'font size': {'smallest': {}, 'small': {}, 'medium': {}, 'large': {}, 'largest': {}},
                    'theme': {'bright': {}, 'dark': {}},
                    'readonly': {'yes': {_id: 'readonly_yes'}, 'no': {_id: 'readonly_no'}},
                    'line numbers': {'yes': {_id: 'numbers_yes'}, 'no': {_id: 'numbers_no'}},
                    'line wrap': {'yes': {}, 'no': {}}
                }
            },
            title: 'untitled',
            searchShow: false,
            pattern: '',
            openShown: false,
            saveShown: false,
            documents: []
        }
    },
    mounted(){
        let ta = document.querySelector('textarea');
        this.codemirror = CodeMirror.fromTextArea(ta);
        this.storage=new DocumentStorage();
        this.uploader=document.getElementById('uploader');
        this.uploader.addEventListener('change',event=>{
            let file=event.target.files[0];
            this.title=file.name;
            let reader = new FileReader();
            reader.readAsText(file);
            reader.onload=event=>{
                let text=event.currentTarget.result;
                this.codemirror.doc.setValue(text);
            };
        });
        this.reset();
    },
    methods:{
        onMenuClick(entry){
            switch(entry){
                case 'open':                                    //file
                    this.openOpen();
                    this.$refs.menu.collapse(); break;
                case 'save':
                    this.save();
                    this.$refs.menu.collapse(); break;
                case 'save as':
                    this.openSave();
                    this.$refs.menu.collapse(); break;
                case 'import':
                    this.uploader.click();
                    this.$refs.menu.collapse(); break;
                case 'export':
                    this.export();
                    this.$refs.menu.collapse(); break;
                case 'reset':
                    this.reset();
                    this.$refs.menu.collapse(); break;
                case 'delete':
                    this.del();
                    this.$refs.menu.collapse(); break;
                case 'find':                                     //edit
                    this.openSearch();
                    this.$refs.menu.collapse(); break;
                case 'yes':
                    this.codemirror.setOption('lineWrapping', true);
                    this.$refs.menu.collapse(); break;
                case 'no':
                    this.codemirror.setOption('lineWrapping', false);
                    this.$refs.menu.collapse(); break;
                case 'Plain':
                    this.codemirror.setOption('mode', 'text/plain');
                    this.$refs.menu.collapse(); break;
                case 'C-like':
                    this.codemirror.setOption('mode', 'text/x-csrc');
                    this.$refs.menu.collapse(); break;
                case 'CSS':
                    this.codemirror.setOption('mode', 'css');
                    this.$refs.menu.collapse(); break;
                case 'HTML':
                    this.codemirror.setOption('mode', 'htmlmixed');
                    this.$refs.menu.collapse(); break;
                case 'JavaScript':
                    this.codemirror.setOption('mode', 'javascript');
                    this.$refs.menu.collapse(); break;
                case 'Markdown':
                    this.codemirror.setOption('mode', 'markdown');
                    this.$refs.menu.collapse(); break;
                case 'PHP':
                    this.codemirror.setOption('mode', 'php');
                    this.$refs.menu.collapse(); break;
                case 'Python':
                    this.codemirror.setOption('mode', 'python');
                    this.$refs.menu.collapse(); break;
                case 'Shell':
                    this.codemirror.setOption('mode', 'shell');
                    this.$refs.menu.collapse(); break;
                case 'SQL':
                    this.codemirror.setOption('mode', 'text/x-sql');
                    this.$refs.menu.collapse(); break;
                case 'smallest':                                    //view
                    this.setFontSize(12);
                    this.$refs.menu.collapse(); break;
                case 'small':
                    this.setFontSize(14);
                    this.$refs.menu.collapse(); break;
                case 'medium':
                    this.setFontSize(16);
                    this.$refs.menu.collapse(); break;
                case 'large':
                    this.setFontSize(18);
                    this.$refs.menu.collapse(); break;
                case 'largest':
                    this.setFontSize(20);
                    this.$refs.menu.collapse(); break;
                case 'bright':
                    this.setTheme(false);
                    this.$refs.menu.collapse(); break;
                case 'dark':
                    this.setTheme(true);
                    this.$refs.menu.collapse(); break;
                case 'readonly_yes':
                    this.codemirror.setOption('readOnly', 'nocursor');
                    this.$refs.menu.collapse(); break;
                case 'readonly_no':
                    this.codemirror.setOption('readOnly', false);
                    this.$refs.menu.collapse(); break;
                case 'numbers_yes':
                    this.codemirror.setOption('lineNumbers', true);
                    this.$refs.menu.collapse(); break;
                case 'numbers_no':
                    this.codemirror.setOption('lineNumbers', false);
                    this.$refs.menu.collapse(); break;
            }
        },
        setTheme(isDark){
            let body=document.querySelector('body');
            if(isDark){
                this.codemirror.setOption('theme','dracula');
                body.style.setProperty('--bg','#181a26');
                body.style.setProperty('--fg','#eee');
                body.style.setProperty('--sel','#282a36');
            }
            else{
                this.codemirror.setOption('theme','default');
                body.style.setProperty('--bg','#fff');
                body.style.setProperty('--fg','#000');
                body.style.setProperty('--sel','#f9f9f9');
            }
        },
        setFontSize(px){
            let codemirror=document.querySelector('.CodeMirror');
            codemirror.style.fontSize=`${px}px`;
            this.codemirror.refresh();
        },
        search(){
            function convert(EOLs,index){
                let ln=0, lastEOL=-1;
                for(let EOL of EOLs){
                    if(EOL<index){
                        ln++;
                        lastEOL=EOL;
                    }
                    else
                        break;
                }
                return {line:ln,ch:index-lastEOL-1};
            };
            let text=this.codemirror.getValue();
            let match=null, selections=[], EOLs=[];
            let regexp=new RegExp('\n','g');
            while(match=regexp.exec(text))
                EOLs.push(match.index);
            regexp=new RegExp(this.pattern,'gi');
            let counter=0;
            while((match=regexp.exec(text))&&(counter++<100))
                selections.push({
                    anchor: convert(EOLs, match.index),
                    head: convert(EOLs, match.index + match[0].toString().length)
                });
            this.codemirror.doc.setSelections(selections);
        },
        openSearch(){
            this.searchShow=true;
            setTimeout(()=>document.getElementById('pattern').focus(),200);
            this.cursor=this.codemirror.doc.getCursor();
        },
        closeSearch(){
            this.searchShow=false;
            this.codemirror.doc.setCursor(this.cursor);
            this.codemirror.focus();
        },
        openSave(){
            setTimeout(()=>document.getElementById('saveName').focus(),200);
            this.saveShown=true;
        },
        save(){
            let text=this.codemirror.getValue();
            let date=new Date();
            this.storage.save(this.title, text, date.toLocaleDateString(), text.length);
            this.saveShown=false;
        },
        reset(){
            this.title="untitled";
            this.codemirror.doc.setValue('');
            this.codemirror.focus();
        },
        del(){
            this.storage.del(this.title);
            this.reset();
        },
        export(){
            let link = document.createElement('a');
            link.href = 'data:text/plain;charset=utf-8,'+this.codemirror.getValue();
            link.download = this.title;
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        },
        async openOpen(){
            this.openShown=true;
            this.documents= await this.storage.all();
        },
        async open(name){
            this.title=name;
            let document= await this.storage.open(name);
            this.codemirror.doc.setValue(document.text);
            this.openShown=false;
        }
    }
})