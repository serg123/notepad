export default {
    template: `
    <div class="modal" :style="{display:shown?'block':'none'}">
        <div class="modal-head">
            <div class="modal-title">{{title}}</div>
            <div class="btn-close" @click="$emit('close')">x</div>
        </div>
        <div class="modal-body">
            <slot>modal body</slot>
        </div>
    </div>
    `,
    props:['shown','title']
};