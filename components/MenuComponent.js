export default {
    template: `
    <nav>
        <div class="m1" v-for="(value,key) in items" @click="onClick($event,value._id||key)">{{key}}
            <div v-if="submenu" class="m2" v-for="(value,key) in value" @click="onClick($event,value._id||key)">{{key}}
                <div class="m3" v-for="(value,key) in value" @click="onClick($event,value._id||key)">{{key}}</div>
            </div>
        </div>
        <div class="title">{{title}}</div>
    </nav>
    `,
    data(){
        return {submenu:true}
    },
    methods:{
        onClick(event,key){
            this.$emit('click',key);
            event.stopPropagation();
        },
        collapse(){
            this.submenu=false;
            setTimeout(()=>this.submenu=true,200);
        }
    },
    props: ['items','title']
};