export default class DocumentStorage{
    constructor(){
        this.db = new Dexie("document_storage");
        this.db.version(1).stores({
            documents: 'name,[name+modified+size]'  //compound keys are not supported in edge (use .each() to return doc parts)
        });
    }
    save(name, text, modified, size){
        return this.db.documents.put({name, text, modified, size});
    }
    del(name){
        return this.db.documents.delete(name);
    }
    open(name){
        return this.db.documents.get(name);
    }
    all(name){
        return this.db.documents.where('[name+modified+size]')
            .between([Dexie.minKey,Dexie.minKey,Dexie.minKey],[Dexie.maxKey,Dexie.maxKey,Dexie.maxKey])
            .keys().then(keys=>keys.map( key=>({name:key[0],modified:key[1],size:key[2]}) ));
    }
}