# Description

### A text editor that allows:
- Edit text documents + highlight syntax (using CodeMirror)
- Find text using JS regular expressions
- Change font size, theme and some other native CodeMirror properties
- CRUD documents using IndexedDB (via Dexie.js)
- Upload/download documents from/to local drive
